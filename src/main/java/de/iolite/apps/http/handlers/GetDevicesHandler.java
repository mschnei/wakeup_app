package de.iolite.apps.http.handlers;

import de.iolite.app.api.environment.Device;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.devices.DeviceProvider;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class GetDevicesHandler extends JSONResponseHandler {

    public GetDevicesHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {
        JSONObject jsonObject = obtainJSONObject(200);
        
        String id = request.getParameter("roomid");

        JSONArray jsonArray = new JSONArray();
        
        //if room id is null, then return all device of Home
        if (id == null) {
            List<de.iolite.app.api.device.access.Device> devices = wakeupApp.getDeviceAPI().getDevices();
            for (de.iolite.app.api.device.access.Device device : devices) {

                JSONObject deviceObj = new JSONObject();

                deviceObj.put("id", device.getIdentifier());
                deviceObj.put("profile_id", device.getProfileIdentifier());
                deviceObj.put("name", device.getName());
                deviceObj.put("manufacturer", device.getManufacturer());
                deviceObj.put("model_name", device.getModelName());

                jsonArray.put(deviceObj);
            }
        } else { // else show all device of this room  
            DeviceProvider deviceProvider = wakeupApp.getDeviceProvider();
            
            // return error if room not found
            if (deviceProvider.listDeviceForRoom(id) == null) {
                return obtainJSONObject(301, "room with id : " + id + "not found");
            }

            for (Device device : deviceProvider.listDeviceForRoom(id)) {
                JSONObject deviceObj = new JSONObject();

                deviceObj.put("id", device.getIdentifier());

                jsonArray.put(deviceObj);
            }
        }

        jsonObject.put("devices", jsonArray);

        return jsonObject;
    }
}
