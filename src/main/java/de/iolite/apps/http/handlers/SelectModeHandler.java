package de.iolite.apps.http.handlers;

import de.iolite.app.api.storage.StorageAPIException;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import de.iolite.apps.WakeupApp;
import org.json.JSONException;
import org.json.JSONObject;

public class SelectModeHandler extends JSONResponseHandler {

    public SelectModeHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        JSONObject jsonObject = obtainJSONObject(200);

        if (request.getParameter("clock_mode") == null) {
            return obtainJSONObject(301, "clock mode is requested");
        }

        if (request.getParameter("bedroom_id") == null) {
            return obtainJSONObject(302, "bedroom_id is requested");
        }

        int newMode = Integer.parseInt(request.getParameter("clock_mode"));
        String roomid = request.getParameter("bedroom_id");
        WakeupApp.LOGGER.info(newMode + ":" + roomid);

        long t1 = 0, t2 = 0;
        if (newMode == 2) {
            long startMS = Long.parseLong(request.getParameter("startms"));
            long endMS = Long.parseLong(request.getParameter("endms"));

          long current = System.currentTimeMillis();
          WakeupApp.LOGGER.info("===============================================================");
          WakeupApp.LOGGER.info("current MS:" + current);
          WakeupApp.LOGGER.info("start MS:" + startMS);
          WakeupApp.LOGGER.info("end MS:" + endMS);
          WakeupApp.LOGGER.info("start Delay:" + (startMS - current));
          WakeupApp.LOGGER.info("end Delay:" + (endMS - current));
          WakeupApp.LOGGER.info("===============================================================");

          if (startMS > endMS) {
                return obtainJSONObject(303, "startMS > endMS");
            }

          if (startMS < current) {
                return obtainJSONObject(303, "startMS < currentTime");
            }

            t1 = startMS - current;
            t2 = endMS - current;
        }

        try {
            jsonObject.put("status", "success");
            wakeupApp.setMode(newMode, roomid, t1, t2);
        } catch (StorageAPIException e) {
            e.printStackTrace();
            jsonObject.put("error", e.toString());
        }

        return jsonObject;
    }
}
