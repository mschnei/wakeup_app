package de.iolite.apps.http.handlers;

import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.app.api.device.access.DeviceProperty;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.devices.MusicPlayer;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;


public class MusicPlayerHandler extends JSONResponseHandler {

    public MusicPlayerHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        String newStatus = request.getParameter("status");
        String musicplayer = request.getParameter("musicplayerid");

        if (musicplayer == null) {
            return obtainJSONObject(301, "musicplayer id is requested");
        }

        JSONObject jsonObject = obtainJSONObject(200);

        MusicPlayer musicPlayer = new MusicPlayer(wakeupApp.getDeviceAPI());

        de.iolite.app.api.device.access.Device device = musicPlayer.findById(musicplayer);

        if (device == null) {
            return obtainJSONObject(304, "device not found");
        }

        if (newStatus == null) {
            wakeupApp.LOGGER.info("------");

            wakeupApp.LOGGER.info(device.getName());
            wakeupApp.LOGGER.info(device.getIdentifier());
            wakeupApp.LOGGER.info(device.getModelName());
            wakeupApp.LOGGER.info(device.getProfileIdentifier());

                for (DeviceProperty<?, ?> o : device.getProperties()) {
                    wakeupApp.LOGGER.info(
                            o.getPropertyType().getIdentifier() + "->" + o.getKey() + ":" + o.getValue()
                    );
            }

            wakeupApp.LOGGER.info("------");

                jsonObject.put("status" , MusicPlayer.getStatus(device));
        } else {
            try {
                if (newStatus.equals("on")) {
                    newStatus = "on";
                }
                if (newStatus.equals("off")) {
                    newStatus = "off";
                }
                MusicPlayer.switchStatus(device, newStatus);
                jsonObject.put("status", "ok");
            } catch (DeviceAPIException e) {
                e.printStackTrace();
                return obtainJSONObject(303, e.getMessage());
            }
        }

        return jsonObject;
    }
}
