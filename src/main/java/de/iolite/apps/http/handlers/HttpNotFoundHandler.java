package de.iolite.apps.http.handlers;

import de.iolite.app.api.frontend.util.FrontendAPIRequestHandler;
import de.iolite.common.requesthandler.HTTPStatus;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import de.iolite.common.requesthandler.IOLITEHTTPResponse;
import de.iolite.common.requesthandler.IOLITEHTTPStaticResponse;


public class HttpNotFoundHandler extends FrontendAPIRequestHandler {

    private static final FrontendAPIRequestHandler fa = new HttpNotFoundHandler();

    private HttpNotFoundHandler() {}

    @Override
    protected IOLITEHTTPResponse handleRequest(final IOLITEHTTPRequest iolitehttpRequest, final String s) {
        return new IOLITEHTTPStaticResponse(HTTPStatus.NOT_FOUND, IOLITEHTTPResponse.HTML_CONTENT_TYPE);
    }

    public static FrontendAPIRequestHandler get() {
        return fa;
    }

}
