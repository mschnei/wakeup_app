package de.iolite.apps.http.handlers;

import de.iolite.app.api.storage.StorageAPIException;
import de.iolite.apps.WakeupApp;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONException;
import org.json.JSONObject;

import static de.iolite.apps.http.handlers.getConfigHandler.KEY_MAX_TEMP;
import static de.iolite.apps.http.handlers.getConfigHandler.KEY_MIN_TEMP;

public class setConfigHandler extends JSONResponseHandler {

  public setConfigHandler(WakeupApp wakeupApp) {
    super(wakeupApp);
  }

  @Override
  protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

    int min = 0, max = 0;
    min = Integer.parseInt(request.getParameter("min"));
    max = Integer.parseInt(request.getParameter("max"));

    try {
      wakeupApp.getStorageAPI().saveInt(KEY_MAX_TEMP, max);
    } catch (StorageAPIException e) {
      e.printStackTrace();
      return obtainJSONObject(301, "success");
    }

    try {
      wakeupApp.getStorageAPI().saveInt(KEY_MIN_TEMP, min);
    } catch (StorageAPIException e) {
      e.printStackTrace();
      return obtainJSONObject(301, "error");
    }

    return obtainJSONObject(200);

  }
}
