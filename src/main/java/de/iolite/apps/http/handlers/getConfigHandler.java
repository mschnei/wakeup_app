package de.iolite.apps.http.handlers;

import de.iolite.app.api.storage.StorageAPIException;
import de.iolite.apps.WakeupApp;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONException;
import org.json.JSONObject;

public class getConfigHandler extends JSONResponseHandler {

  public static String KEY_MAX_TEMP = "key_max_temp";
  public static String KEY_MIN_TEMP = "key_min_temp";

  public getConfigHandler(WakeupApp wakeupApp) {
    super(wakeupApp);
  }

  @Override
  protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {
    int min = 0, max = 0;

    try {
      min = wakeupApp.getStorageAPI().loadInt(KEY_MIN_TEMP);
    } catch (StorageAPIException e) {
      e.printStackTrace();
      min = 10;
    }

    try {
      max = wakeupApp.getStorageAPI().loadInt(KEY_MAX_TEMP);
    } catch (StorageAPIException e) {
      e.printStackTrace();
      max = 23;
    }

    JSONObject jsonObject = obtainJSONObject(200);
    jsonObject.put("max", max);
    jsonObject.put("min", min);

    return jsonObject;
  }
}
