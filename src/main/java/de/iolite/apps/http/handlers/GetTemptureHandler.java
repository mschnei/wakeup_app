package de.iolite.apps.http.handlers;

import de.iolite.app.api.device.access.Device;
import de.iolite.app.api.device.access.DeviceDoubleProperty;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.devices.TemptureSensor;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;


public class GetTemptureHandler extends JSONResponseHandler {

    private final String propty = "http://iolite.de#currentEnvironmentTemperature";

    public GetTemptureHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {
        JSONObject jsonObject = obtainJSONObject(200);

        TemptureSensor temptureSensor = new TemptureSensor(wakeupApp.getDeviceAPI());

        if (request.getParameter("deviceid") == null) {
            return obtainJSONObject(301, "deviceid request");
        }

        Device device = temptureSensor.findById(request.getParameter("deviceid"));

        DeviceDoubleProperty temp = device.getDoubleProperty(propty);

        jsonObject.put("temperature", temp.getValue());

        return jsonObject;
    }
}
