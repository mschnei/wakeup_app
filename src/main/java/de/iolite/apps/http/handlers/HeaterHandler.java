package de.iolite.apps.http.handlers;

import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.devices.Heater;
import de.iolite.apps.devices.Windows;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONException;
import org.json.JSONObject;


public class HeaterHandler extends JSONResponseHandler {

    public HeaterHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        String newtmp = request.getParameter("tmp");
        String heater = request.getParameter("heaterid");

        if (heater == null) {
            return obtainJSONObject(301, "heaterid is requested");
        }

        JSONObject jsonObject = obtainJSONObject(200);

        Heater windows = new Heater(wakeupApp.getDeviceAPI());

        de.iolite.app.api.device.access.Device device = windows.findById(heater);

        if (device == null) {

            return obtainJSONObject(301, "device with id : {" + heater + "} not found");
        }

        if (newtmp == null) {

            jsonObject.put("status" , Heater.heatingTemperatureSetting(device));
        } else {

            try {
                Heater.heatingTemperatureSetting(device, newtmp.equals("true"));
                jsonObject.put("status", "ok");

            } catch (DeviceAPIException e) {
                e.printStackTrace();
                return obtainJSONObject(303, e.getMessage());
            }
        }

        return jsonObject;
    }
}
