package de.iolite.apps.http.handlers;

import de.iolite.app.api.device.access.Device;
import de.iolite.app.api.device.access.DeviceDoubleProperty;
import de.iolite.app.api.device.access.DeviceProperty;
import de.iolite.apps.WakeupApp;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;


public class GetWeatherHandler extends JSONResponseHandler {

    private final String sensorId = "http://iolite.de#WeatherStation";
    private final String propty = "http://iolite.de#currentEnvironmentTemperature";

    public GetWeatherHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        JSONObject jsonObject = new JSONObject();

        Iterator<Device> iterator = wakeupApp.getDeviceAPI().getDevices().iterator();

        List<DeviceProperty<?, ?>> temp = null;

        while (iterator.hasNext()) {

            Device device = iterator.next();

            if (sensorId.equals(device.getProfileIdentifier())) {
                temp = device.getProperties();
                break;
            }
        }

        if (temp == null) {
            jsonObject.put("error", "no temperature sensor found");
        } else {

            for (DeviceProperty<?, ?> deviceProperty : temp) {
                jsonObject.put(deviceProperty.getKey(), deviceProperty.getValue());
            }
        }

        return jsonObject;
    }
}
