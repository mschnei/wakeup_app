package de.iolite.apps.http.handlers;

import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.app.api.device.access.DeviceProperty;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.devices.Windows;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;


public class WindowHandler extends JSONResponseHandler {

    public WindowHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        String newStatus = request.getParameter("status");
        String window = request.getParameter("windowid");

        if (window == null) {
            return obtainJSONObject(301, "windowid is requested");
        }

        JSONObject jsonObject = obtainJSONObject(200);

        Windows windows = new Windows(wakeupApp.getDeviceAPI());

        de.iolite.app.api.device.access.Device device = windows.findById(window);

        if (device == null) {
            return obtainJSONObject(304, "device not found");
        }

        if (newStatus == null) {
            wakeupApp.LOGGER.info("------");

            wakeupApp.LOGGER.info(device.getName());
            wakeupApp.LOGGER.info(device.getIdentifier());
            wakeupApp.LOGGER.info(device.getModelName());
            wakeupApp.LOGGER.info(device.getProfileIdentifier());

                for (DeviceProperty<?, ?> o : device.getProperties()) {
                    wakeupApp.LOGGER.info(
                            o.getPropertyType().getIdentifier() + "->" + o.getKey() + ":" + o.getValue()
                    );
            }

            wakeupApp.LOGGER.info("------");

                jsonObject.put("status" , Windows.getStatus(device));
        } else {
            try {
                if (newStatus.equals("in")) {
                    newStatus = "moving in";
                }
                if (newStatus.equals("out")) {
                    newStatus = "moving out";
                }
                Windows.switchStatus(device, newStatus);
                jsonObject.put("status", "ok");
            } catch (DeviceAPIException e) {
                e.printStackTrace();
                return obtainJSONObject(303, e.getMessage());
            }
        }

        return jsonObject;
    }
}
