package de.iolite.apps.http.handlers;

import de.iolite.apps.WakeupApp;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * get current mode of bedroom
 */
public class GetModeHandler extends JSONResponseHandler {

    public GetModeHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        JSONObject jsonObject = new JSONObject();

        if (request.getParameter("bedroomid") == null) {
            return obtainJSONObject(302, "bedroomid is request");
        }
        String roomid = request.getParameter("bedroomid");

        jsonObject.put("mode", wakeupApp.getMode(roomid));

        return jsonObject;
    }
}
