package de.iolite.apps.http.handlers;

import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.app.api.device.access.DeviceProperty;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.devices.DimmableLamp;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;


public class DimmableLampHandler extends JSONResponseHandler {

    public DimmableLampHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        String newStatus = request.getParameter("status");
        String dimmablelamp = request.getParameter("dimmablelampid");

        if (dimmablelamp == null) {
            return obtainJSONObject(301, "dimmablelamp id is requested");
        }

        JSONObject jsonObject = obtainJSONObject(200);

        DimmableLamp dimmableLamp = new DimmableLamp(wakeupApp.getDeviceAPI());

        de.iolite.app.api.device.access.Device device = dimmableLamp.findById(dimmablelamp);

        if (device == null) {
            return obtainJSONObject(304, "device not found");
        }

        if (newStatus == null) {
            wakeupApp.LOGGER.info("------");

            wakeupApp.LOGGER.info(device.getName());
            wakeupApp.LOGGER.info(device.getIdentifier());
            wakeupApp.LOGGER.info(device.getModelName());
            wakeupApp.LOGGER.info(device.getProfileIdentifier());

                for (DeviceProperty<?, ?> o : device.getProperties()) {
                    wakeupApp.LOGGER.info(
                            o.getPropertyType().getIdentifier() + "->" + o.getKey() + ":" + o.getValue()
                    );
            }

            wakeupApp.LOGGER.info("------");

                jsonObject.put("status" , DimmableLamp.getStatus(device));
        } else {
            try {
                if (newStatus.equals("on")) {
                    newStatus = "on";
                }
                if (newStatus.equals("off")) {
                    newStatus = "off";
                }
                DimmableLamp.switchStatus(device, newStatus);
                jsonObject.put("status", "ok");
            } catch (DeviceAPIException e) {
                e.printStackTrace();
                return obtainJSONObject(303, e.getMessage());
            }
        }

        return jsonObject;
    }
}
