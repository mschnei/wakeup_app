package de.iolite.apps.http.handlers;

import de.iolite.apps.WakeupApp;
import de.iolite.apps.http.Router;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class APIsHandler extends JSONResponseHandler {

    public APIsHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        JSONObject resp = obtainJSONObject(200);
        JSONArray jsonArray = new JSONArray();

        //get all mappings and put them into json array
        for (String api : Router.getInstance().getMapping().keySet()) {
            jsonArray.put(api);
        }
        resp.put("apis", jsonArray);
        return resp;
    }
}
