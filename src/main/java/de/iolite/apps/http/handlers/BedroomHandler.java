package de.iolite.apps.http.handlers;

import de.iolite.app.api.environment.Location;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.WakeupSystem;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class BedroomHandler extends JSONResponseHandler {

    public BedroomHandler(WakeupApp wakeupApp) {
        super(wakeupApp);
    }

    @Override
    protected JSONObject getObject(IOLITEHTTPRequest request) throws JSONException {

        JSONObject jsonObject = obtainJSONObject(200);

        final JSONArray jsonArray = new JSONArray();

        for (final Location location : wakeupApp.getEnvironmentAPI().getLocations()) {

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("name", location.getName());
            jsonObject1.put("id", location.getIdentifier());
            jsonArray.put(jsonObject1);
        }

        jsonObject.put("bedroom", jsonArray);

        return jsonObject;
    }

}
