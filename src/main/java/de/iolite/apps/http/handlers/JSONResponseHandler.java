package de.iolite.apps.http.handlers;

import org.json.JSONException;
import org.json.JSONObject;

import de.iolite.app.api.frontend.util.FrontendAPIRequestHandler;
import de.iolite.apps.WakeupApp;
import de.iolite.common.requesthandler.HTTPStatus;
import de.iolite.common.requesthandler.IOLITEHTTPRequest;
import de.iolite.common.requesthandler.IOLITEHTTPResponse;
import de.iolite.common.requesthandler.IOLITEHTTPStaticResponse;

/**
 *  class for handling the request and returning JSON
 */
public abstract class JSONResponseHandler extends FrontendAPIRequestHandler {

    protected WakeupApp wakeupApp;

    // default style of an json error  message
    private static final String ERROR_JSON = "{\"code\":\"300\", \"msg\":\"%s\"}";

    public JSONResponseHandler(final WakeupApp wakeupApp) {
        this.wakeupApp = wakeupApp;
    }

    /**
     * handling the request and returning JSON
     */
    @Override
    protected IOLITEHTTPResponse handleRequest(final IOLITEHTTPRequest iolitehttpRequest, final String s) {

        try {
            return new IOLITEHTTPStaticResponse(String.valueOf(getObject(iolitehttpRequest)), IOLITEHTTPResponse.JSON_CONTENT_TYPE);

        } catch (final JSONException e) {
            return new IOLITEHTTPStaticResponse(String.format(ERROR_JSON, e.getMessage()), HTTPStatus.BAD_REQUEST, "text/plain");
        }
    }

    /**
     * getting request and returning JSON object
     */
    protected abstract JSONObject getObject(IOLITEHTTPRequest request) throws JSONException;

    /**
     * obtaining JSON object with error code and message infomation
     */
    public static JSONObject obtainJSONObject(final int errorCode, final String... msg) {

        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", errorCode);

        if (msg != null) {

            // if there are many messages, show all of them
            for (int i = 0; i < msg.length; ++i) {
                jsonObject.put("msg" + i, msg[i]);
            }
        }

        return jsonObject;
    }
}