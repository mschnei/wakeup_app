package de.iolite.apps.http;

import de.iolite.app.api.frontend.FrontendAPI;
import de.iolite.app.api.frontend.FrontendAPIException;
import de.iolite.app.api.frontend.util.FrontendAPIRequestHandler;
import de.iolite.app.api.frontend.util.FrontendAPIUtility;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.http.handlers.HttpNotFoundHandler;
import de.iolite.apps.internals.PageWithEmbeddedSessionTokenRequestHandler;
import de.iolite.common.lifecycle.exception.InitializeFailedException;
import de.iolite.common.requesthandler.IOLITEHTTPRequestHandler;
import de.iolite.common.requesthandler.StaticResources;
import de.iolite.utilities.disposeable.Disposeable;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.Map;


public class DefaultHttpServer implements IHttpServer {

    private FrontendAPI frontendAPI;
    private WakeupApp app;

    // assets for the front end
    private Disposeable disposeableAssets;

    private DefaultHttpServer(FrontendAPI frontendAPI, WakeupApp app) {

        this.frontendAPI = frontendAPI;
        this.app = app;
    }

    @Override
    public void initialize() throws FrontendAPIException {

        // go through static assets and register them
        final Map<URI, StaticResources.PathHandlerPair> assets = StaticResources.scanClasspath("assets", getClass().getClassLoader());
        this.disposeableAssets = FrontendAPIUtility.registerPublicHandlers(this.frontendAPI, assets);

        final IOLITEHTTPRequestHandler indexPageRequestHandler = newTemplate("assets/index.html");

        this.frontendAPI.registerRequestHandler("", indexPageRequestHandler);
        this.frontendAPI.registerRequestHandler("index.html", indexPageRequestHandler);

        this.frontendAPI.registerDefaultRequestHandler(HttpNotFoundHandler.get());

        // mapping
        for (Map.Entry<String, Class<? extends FrontendAPIRequestHandler>> entry : Router.getInstance().getMapping().entrySet()) {
            try {

                this.frontendAPI.registerRequestHandler(entry.getKey(),
                     entry.getValue().getConstructor(WakeupApp.class).newInstance(this.app));

            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
    }

    // load a HTML template as string
    private String loadTemplate(final String templateResource) {
        try {
            return StaticResources.loadResource(templateResource, getClass().getClassLoader());
        }
        catch (final IOException e) {
            throw new InitializeFailedException("Loading templates for the dummy app is failed", e);
        }
    }

    private PageWithEmbeddedSessionTokenRequestHandler newTemplate(@Nonnull String url) {
        return new PageWithEmbeddedSessionTokenRequestHandler(loadTemplate(url));
    }
    // call before app destroy
    @Override
    public void clear() {
        if (this.disposeableAssets != null) {
            this.disposeableAssets.dispose();
        }
    }

    public static HttpResourceCleaner create(@Nonnull FrontendAPI frontendAPI, WakeupApp app) throws FrontendAPIException {
        return new HttpResourceCleaner(new DefaultHttpServer(frontendAPI, app));
    }

    public static class HttpResourceCleaner {

        IHttpServer iHttpServer;

        public HttpResourceCleaner(@Nonnull IHttpServer httpServer) throws FrontendAPIException {
            iHttpServer = httpServer;
            iHttpServer.initialize();
        }

        public void clear() {
            iHttpServer.clear();
        }
    }
}
