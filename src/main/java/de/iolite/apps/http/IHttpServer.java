package de.iolite.apps.http;

import de.iolite.app.api.frontend.FrontendAPI;
import de.iolite.app.api.frontend.FrontendAPIException;

import javax.annotation.Nonnull;


public interface IHttpServer {
    void initialize() throws FrontendAPIException;
    void clear();
}
