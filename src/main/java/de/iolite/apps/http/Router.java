package de.iolite.apps.http;

import de.iolite.app.api.frontend.util.FrontendAPIRequestHandler;
import de.iolite.apps.devices.Heater;
import de.iolite.apps.http.handlers.*;

import java.util.HashMap;
import java.util.Map;


public class Router {

    private Map<String , Class<? extends FrontendAPIRequestHandler>> map;
    private static Router instance;

    public static Router getInstance() {
        synchronized (Router.class) {
            if (instance == null) instance = new Router();
        }
        return instance;
    }

    private Router() {
        map = new HashMap<>();

        map.put("devices.json", GetDevicesHandler.class);
        map.put("bedroom.json", BedroomHandler.class);
        map.put("apis.json", APIsHandler.class);
        map.put("get-temperature.json", GetTemptureHandler.class);
        map.put("windows.json", WindowHandler.class);
        map.put("heater.json", HeaterHandler.class);
        map.put("weather.json", GetWeatherHandler.class);
        map.put("dimmablelamp.json", DimmableLampHandler.class);


        map.put("get-mode.json", GetModeHandler.class);
        map.put("set-mode.json", SelectModeHandler.class);

        map.put("get-temp.json", getConfigHandler.class);
        map.put("set-temp.json", setConfigHandler.class);

    }

    public Map<String ,Class<? extends FrontendAPIRequestHandler>> getMapping() {
        return map;
    }
}

