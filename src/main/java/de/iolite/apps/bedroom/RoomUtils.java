package de.iolite.apps.bedroom;

import de.iolite.app.api.environment.EnvironmentAPI;
import de.iolite.app.api.environment.Location;


/**
 * RoomUtils
 */
public class RoomUtils {
    public static Location getRoomById(String id, EnvironmentAPI environmentAPI) {
        for (Location location : environmentAPI.getLocations()) {
            if (location.getIdentifier().equals(id)) {
                return location;
            }
        }
        return null;
    }
}
