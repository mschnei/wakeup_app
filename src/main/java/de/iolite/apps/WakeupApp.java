package de.iolite.apps;

import de.iolite.api.IOLITEAPINotResolvableException;
import de.iolite.api.IOLITEAPIProvider;
import de.iolite.api.IOLITEPermissionDeniedException;
import de.iolite.app.AbstractIOLITEApp;
import de.iolite.app.api.device.access.DeviceAPI;
import de.iolite.app.api.environment.EnvironmentAPI;
import de.iolite.app.api.frontend.FrontendAPI;
import de.iolite.app.api.frontend.FrontendAPIException;
import de.iolite.app.api.storage.StorageAPI;
import de.iolite.app.api.storage.StorageAPIException;
import de.iolite.apps.devices.Device;
import de.iolite.apps.devices.DeviceProvider;
import de.iolite.apps.http.DefaultHttpServer;
import de.iolite.apps.worker.AutoMode;
import de.iolite.apps.worker.TimeMode;
import de.iolite.apps.worker.WorkerManager;
import de.iolite.common.lifecycle.exception.CleanUpFailedException;
import de.iolite.common.lifecycle.exception.InitializeFailedException;
import de.iolite.common.lifecycle.exception.StartFailedException;
import de.iolite.common.lifecycle.exception.StopFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;

/**
 * WakeUp App Class
 */

public final class WakeupApp extends AbstractIOLITEApp implements SystemAPIProvider {

    @Nonnull
    public static final Logger LOGGER = LoggerFactory.getLogger(WakeupApp.class);

    private FrontendAPI frontendAPI;
    private StorageAPI storageAPI;
    private DeviceAPI deviceAPI;
    private EnvironmentAPI environmentAPI;

    private DefaultHttpServer.HttpResourceCleaner cleaner;
    // manager of worker
    private WorkerManager wm;
    // manager of device
    private DeviceProvider dp;

    @Override
    protected void cleanUpHook() throws CleanUpFailedException {
        //Empty
        LOGGER.info("[APP] cleanUpHook");
    }

    @Override
    protected void initializeHook() throws InitializeFailedException {
        //Empty
        LOGGER.info("[APP] initializeHook");
    }

    @Override
    protected void startHook(@Nonnull IOLITEAPIProvider ioliteapiProvider) throws StartFailedException {

        LOGGER.info("[APP] startHook");

        try {
            frontendAPI = ioliteapiProvider.getAPI(FrontendAPI.class);
            cleaner = DefaultHttpServer.create(frontendAPI, this);

            storageAPI = ioliteapiProvider.getAPI(StorageAPI.class);
            deviceAPI = ioliteapiProvider.getAPI(DeviceAPI.class);
            environmentAPI = ioliteapiProvider.getAPI(EnvironmentAPI.class);

            wm = WorkerManager.init(ioliteapiProvider.getScheduler());
            dp = DeviceProvider.getInstance(deviceAPI, environmentAPI);

        } catch (IOLITEAPINotResolvableException | IOLITEPermissionDeniedException | FrontendAPIException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void stopHook() throws StopFailedException {
        LOGGER.info("[APP] stopHook");
        if (cleaner != null) cleaner.clear();
        if (wm != null) wm.onDestroy();
    }

    @Override
    public FrontendAPI getFrontendAPI() {
        return frontendAPI;
    }

    @Override
    public StorageAPI getStorageAPI() {
        return storageAPI;
    }

    @Override
    public DeviceAPI getDeviceAPI() {
        return deviceAPI;
    }

    @Override
    public EnvironmentAPI getEnvironmentAPI() {
        return environmentAPI;
    }


    private final String MODE_KEY = "RUN_MODE";
    public static final int MUSIC_MODE = 0;
    public static final int REPEATING_MODE = 1;
    public static final int MODE_ON_OFF = 0;

    public int setMode(int newMode, String bedroomid, long t1, long t2) throws StorageAPIException {
        if (getMode(bedroomid) != newMode) {
            LOGGER.info("switch to new mode");
        }

        //save new status mode
        storageAPI.saveInt(String.format("%s_%s", MODE_KEY, bedroomid), newMode);

        if (newMode == MUSIC_MODE) {
        //run music_mode
            wm.run(new AutoMode(this, bedroomid));
        } else if (newMode == REPEATING_MODE) {
        //checking if there's no running thread
            wm.ready();
            //make sure t2 > t1


            wm.getScheduler().schedule(new TimeMode(this, bedroomid, wm.getRunningId(), true), t1 , TimeUnit.MILLISECONDS);
            wm.getScheduler().schedule(new TimeMode(this, bedroomid, wm.getRunningId(), false), t2 , TimeUnit.MILLISECONDS);
        } else {
            //checking if there's no running thread
            wm.ready();
        }
        return newMode;
    }

    // return the MODE by getting the bedroomid
    public int getMode(String bedroomid) {
        try {
            return storageAPI.loadInt(String.format("%s_%s", MODE_KEY, bedroomid));
        } catch (StorageAPIException e) {
            e.printStackTrace();
            return MODE_ON_OFF;
        }
    }

    public WorkerManager getWorkerManager() {
        return wm;
    }

    public DeviceProvider getDeviceProvider() {
        return dp;
    }

}
