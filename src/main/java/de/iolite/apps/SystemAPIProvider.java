package de.iolite.apps;

import de.iolite.app.api.device.access.DeviceAPI;
import de.iolite.app.api.environment.EnvironmentAPI;
import de.iolite.app.api.frontend.FrontendAPI;
import de.iolite.app.api.storage.StorageAPI;

public interface SystemAPIProvider {
    FrontendAPI getFrontendAPI();
    StorageAPI getStorageAPI();
    DeviceAPI getDeviceAPI();
    EnvironmentAPI getEnvironmentAPI();
}
