package de.iolite.apps.worker;

import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.app.api.device.access.Device;
import de.iolite.app.api.environment.Location;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.devices.Heater;
import de.iolite.utilities.concurrency.scheduler.Scheduler;

import java.util.List;


public class TimeMode implements Runnable  {

    private WakeupApp app;
    private String id;
    private String runid;
    private boolean b;

    public TimeMode(WakeupApp app, String id, String runid, boolean b) {
        this.app = app;
        this.id = id;
        this.runid = runid;
        this.b = b;
    }

    @Override
    public void run() {
        if (app.getWorkerManager().getRunningId().equals(runid)) {
            Heater heater = new Heater(app.getDeviceAPI());

          List<de.iolite.app.api.environment.Device> devices = null;

          for (Location l : app.getEnvironmentAPI().getLocations()) {
            if (l.getIdentifier().equals(id)) {
              devices = l.getDevices();
            }
          }

          if (devices == null) {
            return;
          }


          String deviceid = null;
          for (de.iolite.app.api.environment.Device device : devices) {
            if (heater.findById(device.getIdentifier()) != null) {
              deviceid = device.getIdentifier();
            }
          }

          if (deviceid == null) return ;

            Device device = heater.findById(deviceid);
            if (device != null) {
                try {
                    Heater.heatingTemperatureSetting(device, b);
                } catch (DeviceAPIException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
