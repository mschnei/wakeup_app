package de.iolite.apps.worker;

import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.app.api.environment.Location;
import de.iolite.app.api.storage.StorageAPIException;
import de.iolite.apps.WakeupApp;
import de.iolite.apps.devices.Device;
import de.iolite.apps.devices.Heater;
import de.iolite.apps.devices.TemptureSensor;
import de.iolite.apps.devices.Windows;

import java.util.ArrayList;
import java.util.List;

import static de.iolite.apps.http.handlers.getConfigHandler.KEY_MAX_TEMP;
import static de.iolite.apps.http.handlers.getConfigHandler.KEY_MIN_TEMP;


public class AutoMode implements Runnable {

    private WakeupApp app;
    private String id;
    private ArrayList<de.iolite.app.api.device.access.Device> devicesIdList = new ArrayList();
    private ArrayList<de.iolite.app.api.device.access.Device> devicesIdHeaterList = new ArrayList();

    public AutoMode(WakeupApp app, String id) {
        this.app = app;
        this.id = id;
    }

    public void switchMode(int i) {
        app.LOGGER.info("MODE switcher : swith to mode ");
    }

    public void highTemp() {
        for (de.iolite.app.api.device.access.Device s : devicesIdList) {
          try {
            Windows.switchStatus(s, "moving out");
          } catch (DeviceAPIException e) {
            e.printStackTrace();
          }
        }
        for (de.iolite.app.api.device.access.Device s : devicesIdHeaterList) {
          try {
            Heater.heatingTemperatureSetting(s, false);
          } catch (DeviceAPIException e) {
            e.printStackTrace();
          }
        }
        app.LOGGER.info("MODE switch to : highTemp");
    }

    public void lowTemp() {
      for (de.iolite.app.api.device.access.Device s : devicesIdList) {
        try {
          Windows.switchStatus(s, "moving in");
        } catch (DeviceAPIException e) {
          e.printStackTrace();
        }
      }
      for (de.iolite.app.api.device.access.Device s : devicesIdHeaterList) {
        try {
          Heater.heatingTemperatureSetting(s, true);
        } catch (DeviceAPIException e) {
          e.printStackTrace();
        }
      }
    }

    public void extrem() {
      for (de.iolite.app.api.device.access.Device s : devicesIdList) {
        try {
          Windows.switchStatus(s, "moving in");
        } catch (DeviceAPIException e) {
          e.printStackTrace();
        }
      }
    }

    @Override
    public void run() {

        List<de.iolite.app.api.environment.Device> devices = null;

        for (Location l : app.getEnvironmentAPI().getLocations()) {
            if (l.getIdentifier().equals(id)) {
                devices = l.getDevices();
            }
        }

        if (devices == null) {
            return;
        }

        String deviceid = null;

        TemptureSensor temptureSensor = new TemptureSensor(app.getDeviceAPI());
        Windows windows = new Windows(app.getDeviceAPI());
        Heater heater = new Heater(app.getDeviceAPI());


        for (de.iolite.app.api.environment.Device device : devices) {
            if (temptureSensor.findById(device.getIdentifier()) != null) {
                deviceid = device.getIdentifier();
            }
            if (windows.findById(device.getIdentifier()) != null) {
                this.devicesIdList.add(windows.findById(device.getIdentifier()));
            }
            if (heater.findById(device.getIdentifier()) != null) {
                this.devicesIdHeaterList.add(heater.findById(device.getIdentifier()));
            }
        }

        if (deviceid == null) return;

        while (app.getMode(id) == WakeupApp.MUSIC_MODE) {

            double temp = temptureSensor
                    .findById(deviceid)
                    .getDoubleProperty("http://iolite.de#currentEnvironmentTemperature")
                    .getValue();


            if (temp > gethighTemp()) {
                highTemp();
            } else if (temp < getlowTemp()) {
                lowTemp();
            }

            if (isExtremeWeather()) {
                extrem();
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public int gethighTemp() {
      try {
        return app.getStorageAPI().loadInt(KEY_MAX_TEMP);
      } catch (StorageAPIException e) {
        return  24;
      }
    }

    public int getlowTemp() {
      try {
        return app.getStorageAPI().loadInt(KEY_MIN_TEMP);
      } catch (StorageAPIException e) {
        return 12;
      }
    }

    public boolean isExtremeWeather() {
        return false;
    }
}
