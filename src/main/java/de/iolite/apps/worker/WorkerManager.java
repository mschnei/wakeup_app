package de.iolite.apps.worker;

import de.iolite.utilities.concurrency.scheduler.Scheduler;

import javax.annotation.Nonnull;
import java.util.UUID;

public class WorkerManager {

    private static WorkerManager sWorkerManager;

    private Scheduler scheduler;

    private WorkerManager(@Nonnull Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public void onDestroy() {

    }

    private String running_id;

    public String getRunningId() {
        return running_id;
    }

    public String run(Runnable runnable) {
        ready();
        go(runnable);
        return running_id;
    }

    public String ready() {
        running_id = UUID.randomUUID().toString();
        return running_id;
    }

    public void go(Runnable runnable) {
        scheduler.execute(runnable);
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public static WorkerManager init(@Nonnull Scheduler Scheduler) {

        synchronized (WorkerManager.class) {
            if (sWorkerManager == null) {
                sWorkerManager = new WorkerManager(Scheduler);
            }
        }
        return sWorkerManager;
    }
}
