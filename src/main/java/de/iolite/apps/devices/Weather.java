package de.iolite.apps.devices;

import de.iolite.app.api.device.access.DeviceAPI;

public class Weather extends Device {

    //Current temperature outside of the home
    public static final String PROPERTY_OUTSIDE_ENVIRONMENT_TEMPERATURE = "PROPERTY_OUTSIDE_ENVIRONMENT_TEMPERATURE";
    //Barometric air pressure
    public static final String PROPERTY_AIR_PRESSURE = "PROPERTY_AIR_PRESSURE";
    //% of sky covered with clouds
    public static final String PROPERTY_CLOUDINESS = "PROPERTY_CLOUDINESS";
    //Describes if there are currently extreme weather conditions.
    public static final String PROPERTY_EXTREME_WEATHER = "PROPERTY_EXTREMEWEATHER";
    //Current fog status.
    public static final String PROPERTY_FOG = "PROPERTY_FOG";
    //Current snow fall intensity.
    public static final String PROPERTY_SNOW_INTENSITY = "PROPERTY_SNOWINTENSITY";
    //Today's sunrise time in milliseconds since epoch UTC.
    public static final String PROPERTY_SUNRISE_TIME = "PROPERTY_SUNRISETIME";
    //Today's sunset time in milliseconds since epoch UTC.
    public static final String PROPERTY_SUNSET_TIME = "PROPERTY_SUNSET_TIME";
    //Determines if currently there is a thunderstorm.
    public static final String PROPERTY_THUNDERSTORM = "PROPERTY_THUNDERSTORM";

    public Weather(DeviceAPI deviceAPI) {
        super(deviceAPI);
    }

    @Override
    String getProfileID() {
        return "http://iolite.de#WeatherStation";
    }

    public static String getProperty(de.iolite.app.api.device.access.Device device, String property) {
        return device.getProperty(property).getValue().toString();
    }
}
