package de.iolite.apps.devices;

import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.app.api.device.access.DeviceAPI;
import de.iolite.app.api.environment.EnvironmentAPI;

public class Windows extends Device {

    public Windows(DeviceAPI deviceAPI) {
        super(deviceAPI);
    }

    @Override
    String getProfileID() {
        //windows profile id
        return "http://iolite.de#Blind";
    }

    public static String getStatus(de.iolite.app.api.device.access.Device device) {
        return device.getProperty("http://iolite.de#blindDriveStatus").getValue().toString();
    }

    public static void switchStatus(de.iolite.app.api.device.access.Device device, String status) throws DeviceAPIException {
        device.getProperty("http://iolite.de#blindDriveStatus").requestValueUpdateFromString(status);
    }
}
