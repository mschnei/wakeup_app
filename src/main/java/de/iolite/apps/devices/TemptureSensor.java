package de.iolite.apps.devices;

import de.iolite.app.api.device.access.DeviceAPI;

public class TemptureSensor extends Device {

    public TemptureSensor(DeviceAPI deviceAPI) {
        super(deviceAPI);
    }

    @Override
    String getProfileID() {
        return "http://iolite.de#TemperatureSensor";
    }
}
