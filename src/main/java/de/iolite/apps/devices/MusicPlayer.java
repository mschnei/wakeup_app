package de.iolite.apps.devices;

import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.app.api.device.access.DeviceAPI;
import de.iolite.app.api.environment.EnvironmentAPI;

public class MusicPlayer extends Device {

    public MusicPlayer(DeviceAPI deviceAPI) {
        super(deviceAPI);
    }

    @Override
    String getProfileID() {
        //music player profile id
        return "http://iolite.de#MusicPlayer";
    }

    public static String getStatus(de.iolite.app.api.device.access.Device device) {
        return device.getProperty("http://iolite.de#MusicPlayerDriveStatus").getValue().toString();
    }

    public static void switchStatus(de.iolite.app.api.device.access.Device device, String status) throws DeviceAPIException {
        device.getProperty("http://iolite.de#MusicPlayerDriveStatus").requestValueUpdateFromString(status);
    }
}
