package de.iolite.apps.devices;

import de.iolite.app.api.device.access.DeviceAPI;

/**
 * a type of Device 
 */

public abstract class Device {

    protected DeviceAPI deviceAPI;

    public Device(DeviceAPI deviceAPI) {
        this.deviceAPI = deviceAPI;
    }

    abstract String getProfileID();

    //return a deivce with { id }, deviceID and ProfileID { getProfileID }
    public de.iolite.app.api.device.access.Device findById(String id) {
        for (de.iolite.app.api.device.access.Device device : deviceAPI.getDevices()) {
            if (getProfileID().equals(device.getProfileIdentifier())&& id.equals(device.getIdentifier())) {
                return device;
            }
        }
        //if not found return null
        return null;
    }
}
