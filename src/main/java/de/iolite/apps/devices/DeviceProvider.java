package de.iolite.apps.devices;

import de.iolite.app.api.device.access.DeviceAPI;
import de.iolite.app.api.environment.*;
import de.iolite.apps.bedroom.RoomUtils;

import java.util.List;

/**
 * singleton, utils for Device
 */
public class DeviceProvider {

    private static DeviceProvider sDeviceProvider;

    private DeviceAPI deviceAPI;
    private EnvironmentAPI environmentAPI;

    private DeviceProvider(DeviceAPI deviceAPI, EnvironmentAPI environmentAPI) {
        this.deviceAPI = deviceAPI;
        this.environmentAPI = environmentAPI;
    }

    //return all device for Room with { roomid }
    public List<de.iolite.app.api.environment.Device> listDeviceForRoom(String roomid) {
        Location location = RoomUtils.getRoomById(roomid, environmentAPI);
        if (location == null) return null;
        return location.getDevices();
    }
    
    //return a signleton of Device Provider instance
    public static DeviceProvider getInstance(DeviceAPI deviceAPI, EnvironmentAPI environmentAPI) {
        synchronized (DeviceProvider.class) {
            if (sDeviceProvider == null) sDeviceProvider = new DeviceProvider(deviceAPI, environmentAPI);
        }

        return sDeviceProvider;
    }
}
