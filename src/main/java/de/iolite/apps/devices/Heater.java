package de.iolite.apps.devices;


import de.iolite.app.api.device.DeviceAPIException;
import de.iolite.app.api.device.access.DeviceAPI;
import de.iolite.app.api.device.access.DeviceProperty;

public class Heater extends Device {

    public Heater(DeviceAPI deviceAPI) {
        super(deviceAPI);
    }

    @Override
    String getProfileID() {
        return "http://iolite.de#Heater";
    }
    
    //switch on/off heater
    public static void heatingTemperatureSetting(de.iolite.app.api.device.access.Device device, boolean to) throws DeviceAPIException {
        if (device != null) {
            device.getProperty("http://iolite.de#valveStatus")
                    .requestValueUpdateFromString(String.valueOf(to));
        }
    }
    
    //get the on/off status of heater
    public static String heatingTemperatureSetting(de.iolite.app.api.device.access.Device device) {
        if (device != null) {
            return String.valueOf(device.getProperty("http://iolite.de#valveStatus").getValue());
        }
        return "false";
    }
}
