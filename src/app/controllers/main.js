//main.js
angular
.module('app')
.controller('roomCtrl', roomCtrl)
.controller('weatherCtrl', weatherCtrl)
.controller('sideNav', sideNav)
.controller('allDevicesCtrl', allDevicesCtrl)
.controller('settingCtrl', settingCtrl);

//convert Hex to RGBA
function convertHex(hex,opacity){
  hex = hex.replace('#','');
  r = parseInt(hex.substring(0,2), 16);
  g = parseInt(hex.substring(2,4), 16);
  b = parseInt(hex.substring(4,6), 16);

  result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
  return result;
}

allDevicesCtrl.$inject['$scope', '$rootScope'];
function allDevicesCtrl($scope, $rootScope) {
  $scope.allDevices = $rootScope.allDevices;
}

settingCtrl.$inject['$scope', '$http'];
function settingCtrl($scope, $http) {

    $scope.max = 24;
    $scope.min = 12;

    $http.get('get-temp.json').then(function onSuccess(response) {
              $scope.max = response.data.max;
              $scope.min = response.data.min;
              }, 
              function onFailure(response) {
            		$scope.content =  '[ERROR] ' + response.statusText + '</br>' + $scope.content;
            		console.log(response);
      });

    $scope.submit = function() {
          $http.get('set-temp.json?max=' + $scope.max.toString() +'&min='+$scope.min.toString()).then(
            function onSuccess(response) {
              
            }, 
      function onFailure(response) {
      });
    }
}

sideNav.$inject['$scope', '$http'];
function sideNav($scope, $http) {

  $scope.rooms = [];

  $http.get('rooms.json').then(function onSuccess(response) {
	            $scope.rooms = response.data.rooms;
              }, 
              function onFailure(response) {
            		$scope.content =  '[ERROR] ' + response.statusText + '</br>' + $scope.content;
            		console.log(response);
      });
}

roomCtrl.$inject['$scope', '$http', '$rootScope', "$interval"];
function roomCtrl($scope, $http, $stateParams, $rootScope, $interval) {

  $scope.classList = [
'primary',
'success',
'info',
'warning',
'danger'];
$scope.rand = function() {
  return $scope.classList[parseInt(Math.random() * 5)];
}

  $scope.room = {
     id : $stateParams.id,
     name : $stateParams.name
  };

  $scope.mode = 0;
  $scope.st = 0;
  $scope.et = 0;
  $scope.devices = [];
  $scope.windows = [];
  $scope.temps = [];
  $scope.heaters = [];
  $scope.wstatus = {};
  $scope.tstatus = {};
  $scope.hstatus = {};

  $scope.notmp = function() {
    return  (Array.isArray($scope.temps) && $scope.temps.length === 0) 
      || (Object.prototype.isPrototypeOf($scope.temps) && Object.keys($scope.temps).length === 0);
  }
  $interval(function(){
  $scope.$watch('mode', function(){
    if ($scope.mode != 2) {
      $scope.setMode($scope.mode, $scope.room.id);
    }
  });
  }, 1000, 1);

  $scope.cw = function(id, status) {
    $http.get('windows.json?windowid=' + id + "&status=" + status).then(function onSuccess(response) {
                if (status == 'in' || status == 'out') {
                  status = "Moving " + status;
                }
                $scope.wstatus[id] = status;
              }, 
              function onFailure(response) {
            		console.log(response);
              });
  }

  $http.get('get-mode.json?roomid=' + $scope.room.id).then(function onSuccess(response) {
	            $scope.mode = response.data.mode;
              }, 
              function onFailure(response) {
            		console.log(response);
              });

  $http.get('devices.json?roomid='+$scope.room.id/*,{params: {roomid:$scope.room.id}}*/).then(function onSuccess(response) {
              $scope.devices = response.data.devices;
              $scope.devices.forEach(function(element) {
                  if ($rootScope.maps.get(element.id) == 'http://iolite.de#Blind') {
                    getWstatus(element);  
                    $scope.windows.push(element);
                  }

                  if ($rootScope.maps.get(element.id) == 'http://iolite.de#TemperatureSensor') {  
                    getTstatus(element);        
                    $scope.temps.push(element);
                  }

                  if ($rootScope.maps.get(element.id) == 'http://iolite.de#Heater') {
                    getHstatus(element); 
                    $scope.heaters.push(element);
                  }

              }, this);
              }, 
              function onFailure(response) {
            		console.log(response);
              });

  var getWstatus = function(element) {
    $http.get('windows.json?windowid='+element.id)
    .then(function onSuccess(response) {
      $scope.wstatus[element.id] = response.data.status;
    });
  }

 var getTstatus = function(element) {
    $http.get('get-temperature.json?deviceid='+element.id)
    .then(function onSuccess(response) {
      $scope.tstatus[element.id] = response.data.temperature;
    });
  }

  var getHstatus = function(element) {
    $http.get('heater.json?heaterid='+element.id)
    .then(function onSuccess(response) {
      $scope.hstatus[element.id] =  response.data.status;
    });
  }

  $scope.setHeater = function(hid, ns) {
    $http.get('heater.json?heaterid='+hid + "&tmp=" + ns)
    .then(function onSuccess(response) {
      $scope.hstatus[hid] = ns;
    });
  }

  $scope.setMode = function(newmode, roomid, st = 0, et = 0) {
    st = Date.parse(st);
    et = Date.parse(et);
      $http.get('set-mode.json?roomid=' + $scope.room.id + "&mode=" + newmode
    + "&startms="+st+"&endms="+et).then(function onSuccess(response) {
              }, 
              function onFailure(response) {
            		console.log(response);
              });
  }

}

weatherCtrl.$inject['$scope', '$http'];
function weatherCtrl($scope, $http, $stateParams) {


  $scope.weather = {};

    $http.get('weather.json').then(function onSuccess(response) {
	            $scope.weather = response.data;
              }, 
              function onFailure(response) {
            	console.log(response);
      });
  
  $scope.getLocalTime = function(nS) {
   return new Date(parseInt(nS)).toLocaleString().replace(/:\d{1,2}$/,' ').split(' ')[1];  
  };
  
  $scope.getStatus = function(weather) {
    let status = [];
    if (weather["http://iolite.de#fog"]== 'true') status.push("fog");
    if (weather["http://iolite.de#thunderstorm"] == 'true') status.push("thunderstorm");
    if (status.length == 0) return ["sunny"]; 
    return status;
  }
}
