import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ClockService } from './clock.service';
//import { MbscModule, mobiscroll } from '@mobiscroll/angular-trial';
//import { mobiscroll } from "../../assets/js/mobiscroll.angular.min.js";
//import "./components/main.js";


declare var libraryVar: any;

   /* mobiscroll.settings = {
      theme: 'ios',
      lang: 'de'
    };*/


@Component({
    selector: 'app-clock',
    templateUrl: './clock.component.html',
    styleUrls: ['./clock.component.css']
})

export class ClockComponent implements OnInit, OnDestroy {

    // //private _clockSubscription: Subscription;
    // time: Date;
    // constructor(private clockService: ClockService) { }

    timer: number;
    timerSettings: any = {
        display: 'inline',
        targetTime: 10,
        maxWheel: 'minutes',
        minWidth: 100,
        onFinish: function () {
            /*mobiscroll.alert({
                title: "ALARM",
                message: "Time to Wake Up!!!"
            });*/
        }
    };


    private _clockSubscription: Subscription;
    time: Date;

    constructor(private clockService: ClockService) { }

    ngOnInit(): void {
        //this._clockSubscription = this.clockService.getClock().subscribe(time => this.time = time);
    }


    ngOnDestroy(): void {
        //this._clockSubscription.unsubscribe();
    }


    startAlarmClock() {

        let jsalarm = {
            padfield: function (f) {
                return (f < 10) ? "0" + f : f
            },
            showcurrenttime: function () {
                let dateobj = new Date();
                let ct = this.padfield(dateobj.getHours()) + ":" + this.padfield(dateobj.getMinutes()) + ":" + this.padfield(dateobj.getSeconds());
                this.ctref.innerHTML = ct;
                this.ctref.setAttribute("title", ct);
                if (typeof this.hourwake != "undefined") { //if alarm is set
                    if (this.ctref.title == (this.hourwake + ":" + this.minutewake + ":" + this.secondwake)) {
                        // clearInterval(jsalarm.timer)
                        // window.location = document.getElementById("musicloc").value
                    }
                }
            },
            init: function () {
                let dateobj = new Date();
                this.ctref = document.getElementById("jsalarm_ct");
                this.submitref = document.getElementById("submitbutton");
                this.submitref.onclick = function () {
                    jsalarm.setalarm();
                    this.value = "Alarm Set";
                    this.disabled = true;
                    return false
                };
                this.resetref = document.getElementById("resetbutton");
                this.resetref.onclick = function () {
                    // jsalarm.submitref.disabled = false;
                    // jsalarm.hourwake = undefined
                    // jsalarm.hourselect.disabled = false
                    // jsalarm.minuteselect.disabled = false
                    // jsalarm.secondselect.disabled = false
                    return false
                };
                let selections = document.getElementsByTagName("select");
                this.hourselect = selections[0];
                this.minuteselect = selections[1];
                this.secondselect = selections[2];
                for (let i = 0; i < 60; i++) {
                    if (i < 24) //If still within range of hours field: 0-23
                        this.hourselect[i] = new Option(this.padfield(i), this.padfield(i), false, dateobj.getHours() == i);
                    this.minuteselect[i] = new Option(this.padfield(i), this.padfield(i), false, dateobj.getMinutes() == i);
                    this.secondselect[i] = new Option(this.padfield(i), this.padfield(i), false, dateobj.getSeconds() == i);

                }
                jsalarm.showcurrenttime();
                // jsalarm.timer = setInterval(function () {
                    jsalarm.showcurrenttime()
                // }, 1000)
            },
            setalarm: function () {
                this.hourwake = this.hourselect.options[this.hourselect.selectedIndex].value;
                this.minutewake = this.minuteselect.options[this.minuteselect.selectedIndex].value;
                this.secondwake = this.secondselect.options[this.secondselect.selectedIndex].value;
                this.hourselect.disabled = true;
                this.minuteselect.disabled = true;
                this.secondselect.disabled = true;
            }
        }
    };

}